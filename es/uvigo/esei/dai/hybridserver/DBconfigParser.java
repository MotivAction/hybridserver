package es.uvigo.esei.dai.hybridserver;


public class DBconfigParser {
	
	private String username;
	private String password;
	private String url;
	private String sgbd;
	private String dbname;
	private String serverName;
	private int port = -1;
	
	public DBconfigParser(String url) {
		this.url = url;
		//jdbc:mysql://localhost:3306/hstestdb
		String[] protocolAndDB = url.split("//");
		sgbd = protocolAndDB[0].split(":")[1];
		String[] HostAndDB = protocolAndDB[1].split("/");
		
		setDbname(HostAndDB[1]);
		if (HostAndDB[0].contains(":")) {
			String[] HostNameAndPort = HostAndDB[0].split(":");
			setServerName(HostNameAndPort[0]);
			setPort(Integer.parseInt(HostNameAndPort[1]));
		} else {
			setServerName(HostAndDB[0]);
		}
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSGBD() {
		return sgbd;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
