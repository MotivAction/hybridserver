package es.uvigo.esei.dai.hybridserver;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class Launcher {
	
	
	
	public static void main(String[] args) {
		
		Properties properties = new Properties();
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(args[0]))))
		{
			String line = "";
			while((line = br.readLine()) != null ) {
				String[] lineProp = line.split("=");
				properties.setProperty(lineProp[0], lineProp[1]);
			}
		} catch (IOException ioe) {
			System.err.println("Error al leer el fichero de configuracion.");
		}
		
		HybridServer httpserver =  new HybridServer(properties);

		httpserver.start();
		
	}
}
