package es.uvigo.esei.dai.hybridserver;

// import static es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus.S200;
// import static es.uvigo.esei.dai.hybridserver.utils.StreamUtils.readToString;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.ws.Endpoint;

public class HybridServer {
	private int servicePort = 8888;
	private Map<String,String> pagesMap;
	private int numClients = 50;
	private Thread serverThread;
	private boolean stop;
	private String webServiceURL;
	private List<ServerConfiguration> p2pServers = null;
	Endpoint ep;
	DBconfigParser dbparsedconfig;
	ExecutorService threadPool;
	
	
	public HybridServer(Map<String, String> pages) {	
		this.pagesMap = pages;

		this.threadPool = Executors.newFixedThreadPool(numClients);
	}
	
	public HybridServer() {

		this.threadPool = Executors.newFixedThreadPool(numClients);
		
	}
	
	public HybridServer(Configuration configuration) {
		this.numClients = configuration.getNumClients();
		this.servicePort = configuration.getHttpPort();
		if (configuration.getWebServiceURL() == "")
			this.webServiceURL = null;
		else
			this.webServiceURL = configuration.getWebServiceURL();
		
		this.p2pServers = configuration.getServers();
		
		this.threadPool = Executors.newFixedThreadPool(numClients);
		
						
		dbparsedconfig = new DBconfigParser(configuration.getDbURL());
		dbparsedconfig.setUsername(configuration.getDbUser());
		dbparsedconfig.setPassword(configuration.getDbPassword());
		ep = Endpoint.create(new HybridServerServiceImpl(this.dbparsedconfig));
	}
	
	public HybridServer(Properties properties) {
		
		this.numClients = Integer.parseInt(properties.getProperty("numClients"));
		this.servicePort = Integer.parseInt(properties.getProperty("port"));
		
		this.threadPool = Executors.newFixedThreadPool(numClients);
		
		dbparsedconfig = new DBconfigParser(properties.getProperty("db.url", "jdbc:mysql://localhost:3306/hstestdb"));
		dbparsedconfig.setUsername(properties.getProperty("db.user", "hsdb"));
		dbparsedconfig.setPassword(properties.getProperty("db.password", "hsdbpass"));
	}
	
	
	
	public int getPort() {
		return servicePort;
	}
	
	public void start() {
		this.serverThread = new Thread() {
			@Override
			public void run() {
				try (final ServerSocket serverSocket = new ServerSocket(servicePort)) {
					
					ServiceTask st;
					while (true) {
						Socket socket = serverSocket.accept();
						if (stop) break;
						if (dbparsedconfig != null) st = new ServiceTask(socket, dbparsedconfig, p2pServers);
						else st = new ServiceTask(socket, pagesMap, p2pServers);
						threadPool.execute(st);
					}
				} catch (IOException e) {
					System.out.println("Error al crear el servidor. " + e.getMessage());
				}
			}
		};

		
		
		this.stop = false;
		this.serverThread.start();
		

		
		if(this.webServiceURL != null)
		{
			ep.setExecutor(this.threadPool);
						
			ep.publish(this.webServiceURL);
			
		}
		
	}
	
	public void stop() {
		this.stop = true;
		
		try (Socket socket = new Socket("localhost", this.servicePort)) {
			// Esta conexión se hace, simplemente, para "despertar" el hilo servidor
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		try {
			this.serverThread.join();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		this.serverThread = null;
		if(this.webServiceURL!=null)
			ep.stop();
		
	}
}
