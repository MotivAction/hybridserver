/**
 *  HybridServer
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.esei.dai.hybridserver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLConfigurationLoader {
	public Configuration load(File xmlFile) throws Exception {
		Document configDocument = loadAndValidateWithInternalXSD(xmlFile);
		System.out.println("Valid");
		Configuration config = new Configuration();
		
		// recogiendo datos del webservice
		config.setHttpPort(Integer.parseInt(configDocument.getElementsByTagName("http").item(0).getTextContent()));
		config.setWebServiceURL(configDocument.getElementsByTagName("webservice").item(0).getTextContent());
		config.setNumClients(Integer.parseInt(configDocument.getElementsByTagName("numClients").item(0).getTextContent()));
		
		// recogiendo datos del database
		config.setDbUser(configDocument.getElementsByTagName("user").item(0).getTextContent());
		config.setDbPassword(configDocument.getElementsByTagName("password").item(0).getTextContent());
		config.setDbURL(configDocument.getElementsByTagName("url").item(0).getTextContent());
		
		// recogiendo datos del database
		NodeList servers = configDocument.getElementsByTagName("server");
		Element server;
		List<ServerConfiguration> sc = new ArrayList<>();
		for (int x = 0; x < servers.getLength(); x++)
		{
			server = (Element) servers.item(x);
			sc.add(new ServerConfiguration(server.getAttribute("name"), 
											server.getAttribute("wsdl"),
											server.getAttribute("namespace"),
											server.getAttribute("service"),
											server.getAttribute("httpAddress")));	 
		}

		config.setServers(sc);
		
		return config;
	}

	
	//TODO
	public static Document loadAndValidateWithInternalXSD(File xmlFile)
			throws ParserConfigurationException, SAXException, IOException {
		// Construcción del parser del documento activando validación
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);
		factory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage",
				XMLConstants.W3C_XML_SCHEMA_NS_URI);
		// Al construir el parser hay que añadir un manejador de errores
		DocumentBuilder builder = factory.newDocumentBuilder();
		builder.setErrorHandler(new SimpleErrorHandler());
		// Parsing y validación del documento
		return builder.parse(xmlFile);
	}



}
