package es.uvigo.esei.dai.hybridserver.dao;

import java.util.HashMap;
import java.util.Map;


public class PagesMapDAO implements PagesDAO {
	private Map<String, String> pages = new HashMap<>();
	
	public PagesMapDAO(Map<String, String> pages) {
		this.pages = pages;
	}
	
	public String getContent(String uuid, DocType type) {
		return this.pages.get(uuid);
	}

	public boolean existPage(String uuid, DocType type) {
		return this.pages.containsKey(uuid);
	}
	
	public String[] getAllPages(DocType type) {
		String[] pagesRet = {};
		
		int x = 0;
		 for (String page : this.pages.keySet())
		 {
			 pagesRet[x++] = page;
		 }
		
		return pagesRet;
	}
	
	public boolean addPage(String uuid, String page, DocType type){
		if (!existPage(uuid, type)) {
			this.pages.put(uuid, page);
			return true;
		} else { 
			return false;
		}
	}

	public boolean deletePage(String uuid, DocType type) {
		if (existPage(uuid, type)) {
			this.pages.remove(uuid);
			return true;
		} else { 
			return false;
		}
	}


}
