package es.uvigo.esei.dai.hybridserver.dao;

import java.sql.SQLException;

public interface PagesXSLTDAOInterface extends PagesDAO {
	
	public String getXSD(String uuid, DocType type) throws SQLException;
	
}
