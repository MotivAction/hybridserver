package es.uvigo.esei.dai.hybridserver.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import es.uvigo.esei.dai.hybridserver.DBconfigParser;
import es.uvigo.esei.dai.hybridserver.jdbc.ConnectionConfiguration;
import es.uvigo.esei.dai.hybridserver.jdbc.ConnectionUtils;
import es.uvigo.esei.dai.hybridserver.jdbc.MySQLConnectionConfiguration;

public class DAO {
	
	private DBconfigParser dbconfigparsed = null;
	private Map<String, String> mapPages = null;
	private DAO singleton = null;
	
	private Connection con;
	
	
	public DAO(DBconfigParser dbconf) { 
		dbconfigparsed = dbconf;
	}
	
	public DAO(Map<String, String> pages) {
		mapPages = new LinkedHashMap<>();
		for(String key: pages.keySet())
			mapPages.put(key, pages.get(key));
	}
	
	public PagesDAO getDAO() throws IllegalStateException, SQLException {
		//Debido a que es un Singleton se sobreescribe con la última instancia creada y es a la que accede continuamente
		if (dbconfigparsed != null) {
			ConnectionConfiguration dbConfig;
			String sgbd = dbconfigparsed.getSGBD();
			switch(sgbd){
				case "mysql":
					dbConfig = new MySQLConnectionConfiguration();
					MySQLConnectionConfiguration dbMySQLConfig = ((MySQLConnectionConfiguration) dbConfig);
							
					dbMySQLConfig.setUserName(dbconfigparsed.getUsername());
					dbMySQLConfig.setPassword(dbconfigparsed.getPassword());
					dbMySQLConfig.setServerName(dbconfigparsed.getServerName());
					dbMySQLConfig.setDbName(dbconfigparsed.getDbname());
					if (dbconfigparsed.getPort() != -1)
						dbMySQLConfig.setPortNumber(dbconfigparsed.getPort());
										
					return createPagesDBDAOMySQL(dbMySQLConfig);
				default:
					throw new IllegalStateException();
			}
		} else if (mapPages != null) {
			return new PagesMapDAO(mapPages);
		} else {
			throw new IllegalStateException();
		}
	}
	
	
	
	private PagesDAO createPagesDBDAOMySQL(MySQLConnectionConfiguration configuration) throws SQLException {		
		PagesDAO dao;
		con = ConnectionUtils.getConnection(configuration);
				
		dao = new PagesXSLTDAO(con);
		
		return dao;
	}
	
	public void closeConnection()
	{
		try {
			this.con.close();
		} catch (SQLException e) {
			System.out.println("Error cerrando la conexión con la base de datos");
		}
	}
}
