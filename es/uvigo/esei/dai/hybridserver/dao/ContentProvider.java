package es.uvigo.esei.dai.hybridserver.dao;

import java.sql.SQLException;

public interface ContentProvider {

	public static enum DocType{
		HTML, XSD, XML, XSLT
	}
	
	String getContent(String uuid, DocType type) throws ContentException, SQLException;

	String[] getAllPages(DocType type) throws SQLException;

}