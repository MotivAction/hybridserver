package es.uvigo.esei.dai.hybridserver.dao;

public class ContentException extends Exception {

	private static final long serialVersionUID = 1L;
	
	
	private String faultInfo;
	
	
	public ContentException(String fault)
	{
		super(fault);
	}
	
	
	public String getFaultInfo(String faultInfo)
	{
		return faultInfo;
	}
	
}
