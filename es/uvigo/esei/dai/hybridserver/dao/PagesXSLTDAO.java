package es.uvigo.esei.dai.hybridserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PagesXSLTDAO extends PagesDBDAO implements PagesXSLTDAOInterface {

	public PagesXSLTDAO(Connection con) {
		super(con);
		
	}
	
	//type en este caso es XSLT
	public String getXSD(String uuid, DocType type) throws SQLException {
		// TODO pensar si hacer la comprobacion de si existe la pagina aqui
		// y si no existe devolver null
		String query = "SELECT xsd FROM " + type.toString() + " WHERE uuid=?";
		String xsd = null;
		
		try (PreparedStatement statement = this.connection.prepareStatement(query))
		{
			
			statement.setString(1, uuid);
			ResultSet rs = statement.executeQuery();
			rs.next();
			xsd = rs.getString("xsd");
			rs.close();
			
		} 
		return xsd;
	}
}
