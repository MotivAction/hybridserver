package es.uvigo.esei.dai.hybridserver.dao;

import java.sql.SQLException;

public interface PagesDAO extends ContentProvider {
	
	public boolean addPage(String uuid, String page, DocType type) throws SQLException;

	public boolean deletePage(String uuid, DocType type) throws SQLException;

}
