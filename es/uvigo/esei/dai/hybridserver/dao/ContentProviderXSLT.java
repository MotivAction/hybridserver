package es.uvigo.esei.dai.hybridserver.dao;

import java.sql.SQLException;

public interface ContentProviderXSLT extends ContentProvider {
	
	public String getUUIDFromXSD(String XSLT) throws IllegalStateException, SQLException, ContentException;

}
