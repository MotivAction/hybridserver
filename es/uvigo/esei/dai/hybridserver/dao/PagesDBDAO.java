package es.uvigo.esei.dai.hybridserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;


public class PagesDBDAO implements PagesDAO {
	
	protected Connection connection;
	
	public PagesDBDAO(Connection con) {
		this.connection = con;
	}

	@Override
	public String getContent(String uuid, DocType type) throws ContentException, SQLException{
		// TODO pensar si hacer la comprobacion de si existe la pagina aqui
		// y si no existe devolver null
		String query = "SELECT content FROM " + type.toString() + " WHERE uuid=?";
		String page = null;
		
		try (PreparedStatement statement = this.connection.prepareStatement(query))
		{
			
			statement.setString(1, uuid);
			ResultSet rs = statement.executeQuery();

			if (rs.next()==false) throw new ContentException("Not found");
			
			page = rs.getString("content");
			
			rs.close();
			
		}
		return page;
	}

	@Override
	public String[] getAllPages(DocType type) throws SQLException {
		String query = "SELECT uuid FROM " + type.toString();
		List<String> pages = new LinkedList<>();
		String[] pagesRet = {};
		
		try (Statement statement = this.connection.createStatement())
		{
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				pages.add(rs.getString("uuid"));
			}
			rs.close();
		} 
		
		if(pages.size() > 0) pagesRet = new String[pages.size()];
		int x = 0;
		for(String page : pages)
		{
			pagesRet[x++] = page;
		}
		
		return pagesRet;
	}

	@Override
	public boolean addPage(String uuid, String page, DocType type) throws SQLException {
		String query = "INSERT INTO " + type.toString() + " (uuid, content) VALUES (?, ?)";
		boolean res = false;
		
		try (PreparedStatement statement = this.connection.prepareStatement(query))
		{
			statement.setString(1, uuid);
			statement.setString(2, page);
			
			if (statement.executeUpdate() == 1)
				res = true;
			
		} 
		return res;
	}

	@Override
	public boolean deletePage(String uuid, DocType type) throws SQLException {
		String query = "DELETE FROM " + type.toString() + " WHERE uuid=?";
		boolean res = false;
		
		try (PreparedStatement statement = this.connection.prepareStatement(query))
		{
			statement.setString(1, uuid);
			
			if (statement.executeUpdate() == 1)
				res = true;
			
		} 
		return res;
	}


}
