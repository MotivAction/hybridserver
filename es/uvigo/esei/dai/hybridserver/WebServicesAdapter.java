package es.uvigo.esei.dai.hybridserver;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;

import es.uvigo.esei.dai.hybridserver.dao.ContentException;
import es.uvigo.esei.dai.hybridserver.dao.ContentProvider.DocType;
import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.PagesXSLTDAO;

public class WebServicesAdapter {

	private List<ServerConfiguration> p2pServers;
	private DAO dao;

	public WebServicesAdapter(List<ServerConfiguration> servers, DAO dao) {
		this.p2pServers = servers;
		this.dao = dao;
	}

	public String[] getAlluuid(ServerConfiguration sc, DocType type) throws SQLException, MalformedURLException {

		HybridServerService hss = getConnection(sc);
		if (hss == null)
			return new String[] {};
		return hss.getAllPages(type);

	}

	public String getContent(String UUID, DocType type) throws ContentException {
		String content = null;

		try {
			PagesXSLTDAO xmlDAO = (PagesXSLTDAO) dao.getDAO();
			content = xmlDAO.getContent(UUID, type);
		} catch (Exception e1) {
			HybridServerService hss;
			int i = 0;

			if (p2pServers == null) {
				throw new ContentException("No servers");
			}

			for (ServerConfiguration sc : p2pServers) {
				try {
					hss = getConnection(sc);
					if (hss == null && (i + 1) == p2pServers.size())
						throw new ContentException("Not found");
					if (hss == null)
						continue;
					content = hss.getContent(UUID, type);
					if (content != null)
						break;
				} catch (ContentException | SQLException | MalformedURLException e) {
					if (++i == p2pServers.size()) {
						throw new ContentException("Not found");
					} else {
						continue;
					}
				}
			}
		}
		dao.closeConnection();

		return content;

	}

	public String getUUIDFromXSD(String xsltuuid) throws ContentException {
		String content = null;
		
		try {
			PagesXSLTDAO xmlDAO = (PagesXSLTDAO) dao.getDAO();
			content = xmlDAO.getXSD(xsltuuid, DocType.XSLT);
		} catch (Exception e1) {
			HybridServerService hss;
			int i = 0;

			if (p2pServers == null) {
				throw new ContentException("No servers");
			}
			
			for (ServerConfiguration sc : p2pServers) {
				try {
					hss = getConnection(sc);
					if (hss == null && (i + 1) == p2pServers.size())
						throw new ContentException("Not found");
					if (hss == null)
						continue;
					content = hss.getUUIDFromXSD(xsltuuid);
					if (content != null)
						break;
				} catch (ContentException ce) {
					if (++i == p2pServers.size()) {
						throw new ContentException("Not found");
					} else {
						continue;
					}
				} catch (SQLException | MalformedURLException e) {
					if (++i < p2pServers.size()) {
						continue;
					}
				}
			}
		}
		dao.closeConnection();

		return content;
	}

	private HybridServerService getConnection(ServerConfiguration sc) throws MalformedURLException {
		HybridServerService hss = null;
		try {
			URL url = new URL(sc.getWsdl());

			QName name = new QName(sc.getNamespace(), sc.getService());

			Service service = Service.create(url, name);
			hss = service.getPort(HybridServerService.class);

		} catch (WebServiceException wse) {
			//System.out.println("No pudo crearse la conexion con: " + sc.getName());
		}

		return hss;
	}

	public List<ServerConfiguration> getServers() {
		return this.p2pServers;
	}
}
