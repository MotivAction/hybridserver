package es.uvigo.esei.dai.hybridserver;

import java.sql.SQLException;

import javax.jws.WebService;

import es.uvigo.esei.dai.hybridserver.dao.ContentException;
import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.PagesDAO;
import es.uvigo.esei.dai.hybridserver.dao.PagesXSLTDAO;

@WebService(endpointInterface="es.uvigo.esei.dai.hybridserver.HybridServerService", serviceName="HybridServerService")
public class HybridServerServiceImpl implements HybridServerService {

	private DAO dao;
	
	public HybridServerServiceImpl(DBconfigParser dbconfigparser) {
		this.dao = new DAO(dbconfigparser);
	}
	
	public String getContent(String uuid, DocType type) throws ContentException, SQLException {
		String content;
		PagesDAO dataPages;

		dataPages = dao.getDAO();

		content = dataPages.getContent(uuid, type);	
		
		dao.closeConnection();

		return content;
	}

	@Override
	public String[] getAllPages(DocType type) throws SQLException {
		
		String[] pages = dao.getDAO().getAllPages(type);
		
		dao.closeConnection();
		
		return pages;

	}


	@Override
	public String getUUIDFromXSD(String XSLT) throws ContentException, SQLException {
		PagesXSLTDAO xsltDAO;
	    String ret = null;
	    xsltDAO = (PagesXSLTDAO) dao.getDAO();

    	ret = xsltDAO.getXSD(XSLT, DocType.XSLT);
	    
	    dao.closeConnection();

	    return ret;
	}

}
