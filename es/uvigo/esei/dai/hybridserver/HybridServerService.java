package es.uvigo.esei.dai.hybridserver;

import java.sql.SQLException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import es.uvigo.esei.dai.hybridserver.dao.ContentException;
import es.uvigo.esei.dai.hybridserver.dao.ContentProviderXSLT;

@WebService
@SOAPBinding(style = Style.RPC)
public interface HybridServerService extends ContentProviderXSLT{

	@WebMethod
	String getContent(String uuid, DocType type) throws ContentException, SQLException;
	@WebMethod
	String[] getAllPages(DocType type) throws SQLException;
	@WebMethod
	String getUUIDFromXSD(String XSLT) throws ContentException, SQLException;

}
