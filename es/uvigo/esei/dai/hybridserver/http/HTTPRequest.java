package es.uvigo.esei.dai.hybridserver.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class HTTPRequest {
	private HTTPRequestMethod method;
	private String resource;
	private String resourceChain;
	private String version;
	private String content = null;
	private Map<String, String> headerParameters = new LinkedHashMap<>();
	private Map<String, String> resourceParameters = new LinkedHashMap<>();

	public HTTPRequest(Reader reader) throws IOException, HTTPParseException {
		BufferedReader bR = new BufferedReader(reader);
		// PARSEANDO CABECERA
		String l = bR.readLine();
		if (l.equals(null))
			throw new HTTPParseException();
		String[] line1 = l.split(" ");
		String clave = "";
		String valor = "";
		
		if (line1.length != 3) {
			throw new HTTPParseException();
		}
		
		// RECOGIENDO METODO
		switch (line1[0]) {
		case "HEAD":
			this.method = HTTPRequestMethod.HEAD;
			break;
		// GET REQUEST ____________________________________________
		case "GET":
			this.method = HTTPRequestMethod.GET;
			break;
		// POST REQUEST ____________________________________________
		case "POST":
			this.method = HTTPRequestMethod.POST;
			break;
		case "PUT":
			this.method = HTTPRequestMethod.PUT;
			break;
		case "DELETE":
			this.method = HTTPRequestMethod.DELETE;
			break;
		case "TRACE":
			this.method = HTTPRequestMethod.TRACE;
			break;
		case "OPTIONS":
			this.method = HTTPRequestMethod.OPTIONS;
			break;
		case "CONNECT":
			this.method = HTTPRequestMethod.CONNECT;
			break;
		default:
			throw (new HTTPParseException());
		}
		
		
		// recogiendo version
		
		if (line1[1].startsWith("/")) {
			this.resourceChain = line1[1];			
		} else {
			throw new HTTPParseException();
		}
		
		
		if (line1[2].equals(HTTPHeaders.HTTP_1_1.getHeader())) {
			this.version = line1[2];
		} else {
			throw new HTTPParseException();
		}

		
		String headerParameterLine = "";
		String[] claveValor;
		// mientras bR.readLine() sea diferente de la linea vacia leemos parametros
		while (!"".equals(headerParameterLine = bR.readLine())) {
			if (headerParameterLine.split(": ").length != 2)
				throw new HTTPParseException();
			clave = headerParameterLine.substring(0, headerParameterLine.indexOf(":"));
			valor = headerParameterLine.substring(headerParameterLine.indexOf(" ") + 1);
			headerParameters.put(clave, valor);
		}

		
		// RECOGIENDO RESOURCE
		
		if (line1[1].startsWith("/")) {
			this.resourceChain = line1[1];			
		} else {
			throw new HTTPParseException();
		}
		if (this.method == HTTPRequestMethod.GET ) {
			// recogiendo resource parameters si los hay
			if (line1[1].contains("?")) {
				// this.resource = line1[1].split("?")[0];
				this.resource = line1[1].substring(1, line1[1].indexOf("?"));
				String stringResourceParameters = line1[1].substring(line1[1].indexOf("?") + 1);

				String[] resourceParametersArray = stringResourceParameters.split("&");

				for (int i = 0; i < resourceParametersArray.length; i++) {
					clave = resourceParametersArray[i].split("=")[0];
					valor = resourceParametersArray[i].split("=")[1];
					this.resourceParameters.put(clave, valor);
				}

			} else {
				this.resource = line1[1].substring(1);
			}
		} else if(this.method == HTTPRequestMethod.POST) {
			int len = Integer.parseInt(headerParameters.get("Content-Length"));

			char[] arrayCharContenido = new char[len];
			bR.read(arrayCharContenido, 0, len);
			String plainContent = new String(arrayCharContenido);

			this.content = URLDecoder.decode(plainContent, "UTF-8");

			// recogemos los parametros del cuerpo ya que es una peticion POST

			this.resource = line1[1].substring(1);

			String[] resourceParametersArray = this.content.split("&");

			for (int i = 0; i < resourceParametersArray.length; i++) {
				clave = resourceParametersArray[i].split("=")[0];
				valor = resourceParametersArray[i].split("=")[1];
				this.resourceParameters.put(clave, valor);
			}
		} else if (this.method ==  HTTPRequestMethod.DELETE) {
			// recogiendo resource parameters si los hay
			if (line1[1].contains("?")) {
				// this.resource = line1[1].split("?")[0];
				this.resource = line1[1].substring(1, line1[1].indexOf("?"));
				String stringResourceParameters = line1[1].substring(line1[1].indexOf("?") + 1);

				String[] resourceParametersArray = stringResourceParameters.split("&");

				for (int i = 0; i < resourceParametersArray.length; i++) {
					clave = resourceParametersArray[i].split("=")[0];
					valor = resourceParametersArray[i].split("=")[1];
					this.resourceParameters.put(clave, valor);
				}

			} else {
				this.resource = line1[1].substring(1);
			}
		}
	}

	public HTTPRequestMethod getMethod() {
		return this.method;
	}

	public String getResourceChain() {
		return this.resourceChain;
	}

	public String[] getResourcePath() {
		String[] resourcePath;
		if (this.resource.equals("")) {
			resourcePath = new String[0];
		} else {
			resourcePath = this.resource.split("/");
		}
		return resourcePath;
	}

	public String getResourceName() {
		// System.out.println(this.resource);
		return this.resource;
	}

	public Map<String, String> getResourceParameters() {
		return this.resourceParameters;
	}

	public String getHttpVersion() {
		return this.version;
	}

	public Map<String, String> getHeaderParameters() {
		return headerParameters;
	}

	public String getContent() {
		return this.content;
	}

	public int getContentLength() {
		if (this.content == null)
			return 0;
		else
			return Integer.parseInt(headerParameters.get("Content-Length"));
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(this.getMethod().name()).append(' ').append(this.getResourceChain())
				.append(' ').append(this.getHttpVersion()).append("\r\n");

		for (Map.Entry<String, String> param : this.getHeaderParameters().entrySet()) {
			sb.append(param.getKey()).append(": ").append(param.getValue()).append("\r\n");
		}

		if (this.getContentLength() > 0) {
			sb.append("\r\n").append(this.getContent());
		}

		return sb.toString();
	}
}
