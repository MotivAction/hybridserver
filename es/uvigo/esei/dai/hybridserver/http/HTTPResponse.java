package es.uvigo.esei.dai.hybridserver.http;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class HTTPResponse {
	
	private String content;
	private HTTPResponseStatus status;
	private String version;
	private Map<String, String> parameters = new LinkedHashMap<String, String>();
	
	public HTTPResponse() {
		this.content = "";
		this.version = "";
	}

	public HTTPResponseStatus getStatus() {
		return this.status;
	}

	public void setStatus(HTTPResponseStatus status) {
		this.status = status;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Map<String, String> getParameters() {
		return this.parameters;
	}

	public String putParameter(String name, String value) {
		return this.parameters.put(name, value);
	}

	public boolean containsParameter(String name) {
		return this.parameters.containsKey(name);
	}

	public String removeParameter(String name) {
		return this.parameters.remove(name);
	}

	public void clearParameters() {
		this.parameters.clear();
	}

	public List<String> listParameters() {
		List<String> parameterList = new LinkedList<String>();
		for(String key : this.parameters.keySet()) {
			parameterList.add(this.parameters.get(key));
		}
		return parameterList;
	}

	public void print(Writer writer) throws IOException {
		
		StringBuilder sb = new StringBuilder();
		
		// version status code and status
		sb.append(this.version + " " + this.status.getCode() + " " + this.status.getStatus() + "\r\n");
		// entity headers
		for(String key : this.parameters.keySet())
			sb.append(key + ": " + this.parameters.get(key) + "\r\n");
		// content-length
		if(this.getContent().length() != 0)
			sb.append("Content-Length: " + this.content.length() + "\r\n");
		// separador de contenido
		sb.append("\r\n");
		sb.append(this.content);
		
		
		writer.write(sb.toString());
		writer.flush();
		
	}

	@Override
	public String toString() {
		final StringWriter writer = new StringWriter();

		try {
			this.print(writer);
		} catch (IOException e) {
		}

		return writer.toString();
	}
}
