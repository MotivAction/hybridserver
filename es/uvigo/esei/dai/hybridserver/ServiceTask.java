package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import es.uvigo.esei.dai.hybridserver.http.HTTPParseException;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequestMethod;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.hybridserver.states.MethodStateDelete;
import es.uvigo.esei.dai.hybridserver.states.MethodStateGet;
import es.uvigo.esei.dai.hybridserver.states.MethodStatePost;


public class ServiceTask implements Runnable {
	
	private Socket socket;
	private DBconfigParser dbconfig;
	private Map<String,String> mapDAO;
	private List<ServerConfiguration> p2p = null;
	
	public ServiceTask(Socket cs, DBconfigParser dbconfigparser, List<ServerConfiguration> p2pServers) {
		this.socket = cs;
		this.dbconfig = dbconfigparser;
		if (p2pServers != null)
			this.p2p = p2pServers;
	}
	
	public ServiceTask(Socket cs, Map<String,String> mapDAO_, List<ServerConfiguration> p2pServers) {
		this.socket = cs;
		this.mapDAO = mapDAO_;
		if (p2pServers != null)
			this.p2p = p2pServers;
	}

	@Override
	public void run() {

		if(!this.socket.isClosed()) {
			OutputStream out; 
			try (Socket actualSocket=this.socket){
				
				Reader reader = new InputStreamReader(actualSocket.getInputStream());
				HTTPRequest request =  new HTTPRequest(reader);
				HTTPRequestMethod method = request.getMethod();
				out = actualSocket.getOutputStream();
				
				HTTPResponse response = new HTTPResponse();
				switch(method){
					case GET:
						response = new MethodStateGet(this.dbconfig, this.p2p).getResponse(request);
						break;
					case POST:
						response = new MethodStatePost(this.dbconfig).getResponse(request);
						break;
					case DELETE:
						response = new MethodStateDelete(this.dbconfig).getResponse(request);
						break;
				}
				out.write(response.toString().getBytes());
				out.flush();
							
			} catch (IOException e) {
				System.out.println("Error connecting with client. " + e.getMessage());
				e.printStackTrace();
			} catch (HTTPParseException e) {
				System.out.println("Error parsing the HTTP request.");
			} catch (IllegalStateException e) {
				System.out.println("Singleton no configurado. " + e.getMessage());
				try(OutputStream output = this.socket.getOutputStream()){
					HTTPResponse response = new HTTPResponse();
					response.setStatus(HTTPResponseStatus.S500);
					response.putParameter("Connection", "close");
					response.putParameter("Content-encoding", "UTF-8");
					output.write(response.toString().getBytes());
					output.flush();
				} catch (IOException ioe) {
					System.out.println("Error al escribir en el socket.");
				}
			} catch (SQLException e) {
				System.out.println("Error al conectar con la BD. " + e.getMessage());
				try(OutputStream output = this.socket.getOutputStream()){
					HTTPResponse response = new HTTPResponse();
					response.setStatus(HTTPResponseStatus.S500);
					response.putParameter("Connection", "close");
					response.putParameter("Content-encoding", "UTF-8");
					output.write(response.toString().getBytes());
					output.flush();
				} catch (IOException ioe) {
					System.out.println("Error al escribir en el socket.");
				} 
			} catch (Exception e)
			{
				try(OutputStream output = this.socket.getOutputStream()){
					HTTPResponse response = new HTTPResponse();
					response.setStatus(HTTPResponseStatus.S500);
					response.putParameter("Connection", "close");
					response.putParameter("Content-encoding", "UTF-8");
					output.write(response.toString().getBytes());
					output.flush();
				} catch (IOException ioe) {
					System.out.println("Error al escribir en el socket.");
				} 
			}
		}
	}
}
