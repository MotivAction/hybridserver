package es.uvigo.esei.dai.hybridserver.states;

import java.sql.SQLException;
import java.util.Map;

import es.uvigo.esei.dai.hybridserver.DBconfigParser;
import es.uvigo.esei.dai.hybridserver.WebServicesAdapter;
import es.uvigo.esei.dai.hybridserver.dao.ContentException;
import es.uvigo.esei.dai.hybridserver.dao.ContentProvider.DocType;
import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.PagesDAO;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;

public class MethodStateDelete implements MethodState {

	private DAO dao;
	private WebServicesAdapter p2p;

	
	public MethodStateDelete(DBconfigParser dbconfigparser) {
		this.dao = new DAO(dbconfigparser);
	}

	public HTTPResponse getResponse(HTTPRequest request) throws IllegalStateException, SQLException {
		Map<String, String> resourceParameters = request.getResourceParameters();
		HTTPResponse response = new HTTPResponse();

		if (request.getResourceName().equals("html") && resourceParameters.containsKey("uuid")) {
			htmlRequestHandler(resourceParameters.get("uuid"), response);
		} else if (request.getResourceName().equals("xml") && resourceParameters.containsKey("uuid")) {
			xmlRequestHandler(resourceParameters.get("uuid"), response);
		} else if (request.getResourceName().equals("xsd") && resourceParameters.containsKey("uuid")) {
			xsdRequestHandler(resourceParameters.get("uuid"), response);
		} else if (request.getResourceName().equals("xslt") && resourceParameters.containsKey("uuid")) {
			xsltRequestHandler(resourceParameters.get("uuid"), response);
		} else {
			response.setStatus(HTTPResponseStatus.S400);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("Invalid resource.");
			response.putParameter("Content-Type", "text/html");
		}
		
		dao.closeConnection();
		return response;
	}

	private void htmlRequestHandler(String uuid, HTTPResponse response) throws IllegalStateException, SQLException {
		PagesDAO dataPages = dao.getDAO();	
		try{
			//Guarro pero precioso, recuperamos para saber si existe
			dataPages.getContent(uuid, DocType.HTML);
			dataPages.deletePage(uuid, DocType.HTML);
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("The page already exists");
		} catch(ContentException ce) {
			response.setStatus(HTTPResponseStatus.S404);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("The page couldn't be deleted");
		}
	}

	private void xmlRequestHandler(String uuid, HTTPResponse response) throws IllegalStateException, SQLException {
		PagesDAO dataPages = dao.getDAO();	
		try{
			//Guarro pero precioso, recuperamos para saber si existe
			dataPages.getContent(uuid, DocType.XML);
			dataPages.deletePage(uuid, DocType.XML);
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("The page already exists");
		} catch(ContentException ce) {
			response.setStatus(HTTPResponseStatus.S404);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("The page couldn't be deleted");
		}
	}

	private void xsdRequestHandler(String uuid, HTTPResponse response) throws IllegalStateException, SQLException {
		PagesDAO dataPages = dao.getDAO();	
		try{
			//Guarro pero precioso, recuperamos para saber si existe
			dataPages.getContent(uuid, DocType.XSD);
			dataPages.deletePage(uuid, DocType.XSD);
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("The page already exists");
		} catch(ContentException ce) {
			response.setStatus(HTTPResponseStatus.S404);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("The page couldn't be deleted");
		}
	}
	
	private void xsltRequestHandler(String uuid, HTTPResponse response) throws IllegalStateException, SQLException {
		PagesDAO dataPages = dao.getDAO();	
		try{
			//Guarro pero precioso, recuperamos para saber si existe
			dataPages.getContent(uuid, DocType.XSLT);
			dataPages.deletePage(uuid, DocType.XSLT);
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("The page already exists");
		} catch(ContentException ce) {
			response.setStatus(HTTPResponseStatus.S404);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("The page couldn't be deleted");
		}
	}

}
