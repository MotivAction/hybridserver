package es.uvigo.esei.dai.hybridserver.states;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import es.uvigo.esei.dai.hybridserver.DBconfigParser;
import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.WebServicesAdapter;
import es.uvigo.esei.dai.hybridserver.dao.ContentException;
import es.uvigo.esei.dai.hybridserver.dao.ContentProvider;
import es.uvigo.esei.dai.hybridserver.dao.ContentProvider.DocType;
import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.PagesXSLTDAO;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;

public class MethodStateGet implements MethodState {

	private static final String WEB_PAGE = "<html><h1>Hybrid Server</h1>" + "<p>Figueiras Gomez, Edgar.</p>"
			+ "<p>Rodriguez Rodriguez, Diego.</p>" + "<a href=\"/html\">Paginas</a>" + "<html>";

	private DAO dao;
	private WebServicesAdapter p2p = null;
	
	public MethodStateGet(DBconfigParser dbconfigparser, List<ServerConfiguration> p2pServers) {
		this.dao = new DAO(dbconfigparser);
		if(p2pServers != null)
			this.p2p = new WebServicesAdapter(p2pServers, this.dao);
	}

	public HTTPResponse getResponse(HTTPRequest request) throws IllegalStateException, SQLException, MalformedURLException {

		HTTPResponse response = new HTTPResponse();
		
		if (request.getResourceName().equals("html")) {
			// function respuesta html
			htmlRequestHandler(request.getResourceParameters(), response);
			dao.closeConnection();
			
		} else if (request.getResourceName().equals("")) {
			// main page
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent(WEB_PAGE);
			response.putParameter("Content-Type", "text/html");
			
		} else if (request.getResourceName().equals("xml")) {
			xmlRequestHandler(request.getResourceParameters(), response);
			dao.closeConnection();

		} else if (request.getResourceName().equals("xsd")) {
			xsdRequestHandler(request.getResourceParameters(), response);
			dao.closeConnection();

		} else if (request.getResourceName().equals("xslt")) {
			xsltRequestHandler(request.getResourceParameters(), response);
			dao.closeConnection();

		} else {
			response.setStatus(HTTPResponseStatus.S400);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("Invalid resource.");
			response.putParameter("Content-Type", "text/html");
		}
		return response;
	}

	private void htmlRequestHandler(Map<String, String> resourceParameters, HTTPResponse response)
			throws SQLException, MalformedURLException {

		response.putParameter("Content-Type", "text/html");
		if (!resourceParameters.containsKey("uuid")) {
			// lista los links a todas las paginas
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());

			ContentProvider dataPages = dao.getDAO();

			StringBuilder listAllPages = new StringBuilder();
			listAllPages.append("<html><h1>Local Server</h1>" + "<ul>");
			for (String uuid : dataPages.getAllPages(DocType.HTML))
				listAllPages.append("<li><a href=\"/html?uuid=" + uuid + "\">" + uuid + "</a></li>");

			
			
			if(p2p != null && p2p.getServers().size() > 0)
			{
				List<ServerConfiguration> servers = p2p.getServers();
				listAllPages.append("</ul>");
				String[] actual; 
				for(ServerConfiguration sc : servers)
				{
					actual = p2p.getAlluuid(sc, DocType.HTML);
					if(actual.length!=0)
					{
						listAllPages.append("<h1>" + sc.getName() + "</h1>" + "<ul>");
						for (String uuid : actual)
							listAllPages.append("<li><a href=\"/html?uuid=" + uuid + "\">" + uuid + "</a></li>");
					}
					
				}
			}
						
			listAllPages.append("</ul></html>");	
			response.setContent(listAllPages.toString());
			
		} else if (resourceParameters.containsKey("uuid")) {
			String uuid = resourceParameters.get("uuid");
			try{
				response.setStatus(HTTPResponseStatus.S200);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				
				if(p2p != null && p2p.getServers().size() > 0){
					response.setContent(p2p.getContent(uuid, DocType.HTML));
				}else{
					ContentProvider dataPages = dao.getDAO();
					response.setContent(dataPages.getContent(uuid, DocType.HTML));
				}
					
					
			}catch (ContentException cee){
				response.setStatus(HTTPResponseStatus.S404);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				response.setContent("No existe el fichero con uuid: " + resourceParameters.get("uuid"));
			}
		}
	}

	private void xmlRequestHandler(Map<String, String> resourceParameters, HTTPResponse response) throws IllegalStateException, SQLException, MalformedURLException{

		response.putParameter("Content-Type", "text/html");
		if (!resourceParameters.containsKey("uuid")) {
			// lista los links a todas las paginas
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());

			ContentProvider dataPages = dao.getDAO();

			StringBuilder listAllPages = new StringBuilder();
			listAllPages.append("<html><h1>Local Server</h1>" + "<ul>");
			for (String uuid : dataPages.getAllPages(DocType.XML))
				listAllPages.append("<li><a href=\"/xml?uuid=" + uuid + "\">" + uuid + "</a></li>");
			
			if(p2p != null && p2p.getServers().size() > 0) {
				List<ServerConfiguration> servers = p2p.getServers();
				listAllPages.append("</ul>");
				String[] actual; 
				for(ServerConfiguration sc : servers)
				{
					actual = p2p.getAlluuid(sc, DocType.XML);
					if(actual.length!=0)
					{
						listAllPages.append("<h1>" + sc.getName() + "</h1>" + "<ul>");
						for (String uuid : actual)
							listAllPages.append("<li><a href=\"/xml?uuid=" + uuid + "\">" + uuid + "</a></li>");
					}
					
				}
			}
			
			listAllPages.append("</ul></html>");
			response.setContent(listAllPages.toString());
			response.putParameter("Content-Type", "text/html");
			
		} else if (resourceParameters.containsKey("uuid") && resourceParameters.containsKey("xslt")) {
			String xmluuid = resourceParameters.get("uuid");
			String xsltuuid = resourceParameters.get("xslt");
			String xsduuid;
			String contentStringXML = null;
			String contentStringXSLT = null;
			String contentStringXSD = null;
			try {
				if(p2p != null && p2p.getServers().size() > 0) {
					contentStringXML = p2p.getContent(xmluuid, DocType.XML);

					contentStringXSLT = p2p.getContent(xsltuuid, DocType.XSLT);
					
					xsduuid = p2p.getUUIDFromXSD(xsltuuid);
					
					contentStringXSD = p2p.getContent(xsduuid, DocType.XSD);
				}else{
					
					PagesXSLTDAO dataPages = (PagesXSLTDAO)dao.getDAO();
					contentStringXML = dataPages.getContent(xmluuid, DocType.XML);

					contentStringXSLT = dataPages.getContent(xsltuuid, DocType.XSLT);
					
					xsduuid = dataPages.getXSD(xsltuuid, DocType.XSLT);
					
					contentStringXSD = dataPages.getContent(xsduuid, DocType.XSD);
					
				}
				
				response.setStatus(HTTPResponseStatus.S200);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				response.setContent(XMLHelper.convertXMLtoHTML(contentStringXML, contentStringXSD, contentStringXSLT));
				response.putParameter("Content-Type", "text/html");

			} catch (SAXException | ParserConfigurationException | IOException | TransformerException e) {
				response.setStatus(HTTPResponseStatus.S400);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			} catch (ContentException cee) {
				response.setStatus(HTTPResponseStatus.S404);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				response.setContent("No existe el fichero con uuid: " + resourceParameters.get("uuid"));
			} catch(Exception exc) {
				
				response.setStatus(HTTPResponseStatus.S500);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			}
			
		} else if (resourceParameters.containsKey("uuid")) {
			String uuid = resourceParameters.get("uuid");
			try{
				response.setStatus(HTTPResponseStatus.S200);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				if(p2p != null && p2p.getServers().size() > 0) {
					response.setContent(p2p.getContent(uuid, DocType.XML));
				}else{
					ContentProvider dataPages = dao.getDAO();
					response.setContent(dataPages.getContent(uuid, DocType.XML));
				}
				response.putParameter("Content-Type", "application/xml");
			}catch(ContentException cee){
				response.setStatus(HTTPResponseStatus.S404);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				response.setContent("No existe el fichero con uuid: " + uuid);
				response.putParameter("Content-Type", "application/xml");
			}catch(Exception e){
				response.setStatus(HTTPResponseStatus.S500);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			}
		}
	}

	private void xsdRequestHandler(Map<String, String> resourceParameters, HTTPResponse response)
			throws IllegalStateException, SQLException, MalformedURLException {

		response.putParameter("Content-Type", "text/html");
		if (!resourceParameters.containsKey("uuid")) {
			// lista los links a todas las paginas
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());

			ContentProvider dataPages = dao.getDAO();

			StringBuilder listAllPages = new StringBuilder();
			listAllPages.append("<html><h1>Local Server</h1>" + "<ul>");
			for (String uuid : dataPages.getAllPages(DocType.XSD))
				listAllPages.append("<li><a href=\"/xsd?uuid=" + uuid + "\">" + uuid + "</a></li>");
			
			if(p2p != null && p2p.getServers().size() > 0) {
				List<ServerConfiguration> servers = p2p.getServers();
				listAllPages.append("</ul>");
				String[] actual;
				for(ServerConfiguration sc : servers)
				{
					actual = p2p.getAlluuid(sc, DocType.XSD);
					if(actual.length!=0)
					{
						listAllPages.append("<h1>" + sc.getName() + "</h1>" + "<ul>");
						for (String uuid : actual)
							listAllPages.append("<li><a href=\"/xsd?uuid=" + uuid + "\">" + uuid + "</a></li>");
					}
					
				}
			}
			
			listAllPages.append("</ul></html>");
			response.setContent(listAllPages.toString());
			response.putParameter("Content-Type", "text/html");
		} else if (resourceParameters.containsKey("uuid")) {
			String uuid = resourceParameters.get("uuid");
			
			try{
				response.setStatus(HTTPResponseStatus.S200);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				if(p2p != null && p2p.getServers().size() > 0) {
					response.setContent(p2p.getContent(uuid, DocType.XSD));
				}else{
					ContentProvider dataPages = dao.getDAO();
					response.setContent(dataPages.getContent(uuid, DocType.XSD));
				}
				
				response.putParameter("Content-Type", "application/xml");

			} catch(ContentException cee) {
				response.setStatus(HTTPResponseStatus.S404);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				response.setContent("No existe el fichero con uuid: " + uuid);
				response.putParameter("Content-Type", "application/xml");
			} catch(Exception exc)
			{
				response.setStatus(HTTPResponseStatus.S500);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			}
		}
	}

	private void xsltRequestHandler(Map<String, String> resourceParameters, HTTPResponse response)
			throws IllegalStateException, SQLException, MalformedURLException {

		response.putParameter("Content-Type", "text/html");
		if (!resourceParameters.containsKey("uuid")) {
			// lista los links a todas las paginas
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());

			ContentProvider dataPages = dao.getDAO();

			StringBuilder listAllPages = new StringBuilder();
			listAllPages.append("<html><h1>Local Server</h1>" + "<ul>");
			for (String uuid : dataPages.getAllPages(DocType.XSLT))
				listAllPages.append("<li><a href=\"/xslt?uuid=" + uuid + "\">" + uuid + "</a></li>");

			
			if(p2p != null && p2p.getServers().size() > 0) {
				List<ServerConfiguration> servers = p2p.getServers();
				listAllPages.append("</ul>");
				String[] actual; 
				for(ServerConfiguration sc : servers)
				{
					actual = p2p.getAlluuid(sc, DocType.XSLT);
					if(actual.length!=0)
					{
						listAllPages.append("<h1>" + sc.getName() + "</h1>" + "<ul>");
						for (String uuid : actual)
							listAllPages.append("<li><a href=\"/xslt?uuid=" + uuid + "\">" + uuid + "</a></li>");
					}
					
				}
			}
			
			
			listAllPages.append("</ul></html>");
			response.setContent(listAllPages.toString());
			response.putParameter("Content-Type", "text/html");
		} else if (resourceParameters.containsKey("uuid")) {
			String uuid = resourceParameters.get("uuid");
			try{
				response.setStatus(HTTPResponseStatus.S200);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				if(p2p != null && p2p.getServers().size() > 0) {
					response.setContent(p2p.getContent(uuid, DocType.XSLT));
				}else{
					ContentProvider dataPages = dao.getDAO();
					response.setContent(dataPages.getContent(uuid, DocType.XSLT));
				}
				response.putParameter("Content-Type", "application/xml");
			} catch(ContentException cee) {
				response.setStatus(HTTPResponseStatus.S404);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				response.setContent("No existe el fichero con uuid: " + uuid);
				response.putParameter("Content-Type", "text/html");
			}
		}
	}
}
