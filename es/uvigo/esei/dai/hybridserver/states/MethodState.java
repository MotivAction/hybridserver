package es.uvigo.esei.dai.hybridserver.states;

import java.net.MalformedURLException;
import java.sql.SQLException;

import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;

/**
 * Esta interfaz se encarga de abstraer el funcionamiento de cada
 * http method ya que todos reciben una request y devuelven una
 * response independientemente de cual sea su funcionamiento
 * interno.
 */

public interface MethodState {
	
	
	/**
	 * Cada implementación deberá encargarse de, según su tipo devolver
	 * siempre una response.
	 */
	
	public HTTPResponse getResponse(HTTPRequest request)
			throws IllegalStateException, SQLException, MalformedURLException;

}
