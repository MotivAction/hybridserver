package es.uvigo.esei.dai.hybridserver.states;

import java.sql.SQLException;
import java.util.UUID;

import es.uvigo.esei.dai.hybridserver.DBconfigParser;
import es.uvigo.esei.dai.hybridserver.dao.ContentException;
import es.uvigo.esei.dai.hybridserver.dao.ContentProvider;
import es.uvigo.esei.dai.hybridserver.dao.ContentProvider.DocType;
import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.PagesDAO;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;

public class MethodStatePost implements MethodState {
	
	private DAO dao;
	
	
	public MethodStatePost(DBconfigParser dbconfigparser) {
		this.dao = new DAO(dbconfigparser);
	}
	
	public HTTPResponse getResponse(HTTPRequest request)
			throws IllegalStateException, SQLException {
		
		HTTPResponse response = new HTTPResponse();
		
		if(!request.getResourceParameters().isEmpty()
				&& request.getResourceParameters().containsKey("html")) {
			
			String newUUID = UUID.randomUUID().toString();
			String content = request.getResourceParameters().get("html");
			htmlRequestHandler(newUUID, content, response);
		} else if(!request.getResourceParameters().isEmpty()
				&& request.getResourceParameters().containsKey("xml")) {
			
			String newUUID = UUID.randomUUID().toString();
			String content = request.getResourceParameters().get("xml");
			xmlRequestHandler(newUUID, content, response);
		} else if(!request.getResourceParameters().isEmpty()
				&& request.getResourceName().equals("xsd")) {
			
			String newUUID = UUID.randomUUID().toString();
			String content = request.getResourceParameters().get("xsd");
			xsdRequestHandler(newUUID, content, response);
		} else if(!request.getResourceParameters().isEmpty()
				&& request.getResourceName().equals("xslt")) {
			
			String newUUID = UUID.randomUUID().toString();
			String content = request.getResourceParameters().get("xslt");
			String xsdUUID = request.getResourceParameters().get("xsd");
			checkXSD(xsdUUID, newUUID, content, response);
		} else {
			response.setStatus(HTTPResponseStatus.S400);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("Invalid content.");
		}
		
		return response;
	}
	
	
	private void htmlRequestHandler(String newUUID, String content, HTTPResponse response)
			throws IllegalStateException, SQLException {
		
		PagesDAO dataPages = dao.getDAO();
		
		try{
			dataPages.addPage(newUUID, content, ContentProvider.DocType.HTML);
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("<a href=\"html?uuid=" + newUUID + "\">" + newUUID + "</a>");
			response.putParameter("Content-Type", "text/html");
		} catch(SQLException sql) {
			response.setStatus(HTTPResponseStatus.S500);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		} finally {
			dao.closeConnection();
		}

	}

	private void xmlRequestHandler(String newUUID, String content, HTTPResponse response)
			throws IllegalStateException, SQLException {
		
		PagesDAO dataPages = dao.getDAO();
		
		try{
			dataPages.addPage(newUUID, content, ContentProvider.DocType.XML);
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("<a href=\"xml?uuid=" + newUUID + "\">" + newUUID + "</a>");
			response.putParameter("Content-Type", "text/html");
		} catch(SQLException sql) {
			response.setStatus(HTTPResponseStatus.S500);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		} finally {
			dao.closeConnection();
		}

	}
	
	private void xsdRequestHandler(String newUUID, String content, HTTPResponse response)
			throws IllegalStateException, SQLException {

		PagesDAO dataPages = dao.getDAO();
		
		try{
			dataPages.addPage(newUUID, content, ContentProvider.DocType.XSD);
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("<a href=\"xsd?uuid=" + newUUID + "\">" + newUUID + "</a>");
			response.putParameter("Content-Type", "text/html");
		} catch(SQLException sql) {
			response.setStatus(HTTPResponseStatus.S500);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		} finally {
			dao.closeConnection();
		}

	}
	
	private void xsltRequestHandler(String newUUID, String content, HTTPResponse response)
			throws IllegalStateException, SQLException {
		
		PagesDAO dataPages = dao.getDAO();
		
		try{
			dataPages.addPage(newUUID, content, ContentProvider.DocType.XSLT);
			response.setStatus(HTTPResponseStatus.S200);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("<a href=\"xslt?uuid=" + newUUID + "\">" + newUUID + "</a>");
			response.putParameter("Content-Type", "text/html");
		} catch(SQLException sql) {
			response.setStatus(HTTPResponseStatus.S500);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		} finally {
			dao.closeConnection();
		}

	}
	
	
	
	private void checkXSD(String xsdUUID, String newUUID, String content, HTTPResponse response) throws IllegalStateException, SQLException{
		
		if (xsdUUID != null) {
			try 
			{
				PagesDAO dataPages = dao.getDAO();
				dataPages.getContent(xsdUUID, DocType.XSD);
				xsltRequestHandler(newUUID, content, response);
				
			} catch(ContentException ce) {
				
				response.setStatus(HTTPResponseStatus.S404);
				response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				response.setContent("No existe el fichero con uuid: " + xsdUUID);
				response.putParameter("Content-Type", "text/html");
				//xsltRequestHandler(newUUID, content, response);
			}  finally {
				dao.closeConnection();
			}
		} else {
			response.setStatus(HTTPResponseStatus.S400);
			response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			response.setContent("Invalid resource.");
			response.putParameter("Content-Type", "text/html");
		}

	}
}
