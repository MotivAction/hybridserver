package es.uvigo.esei.dai.hybridserver.states;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import es.uvigo.esei.dai.hybridserver.SimpleErrorHandler;

public class XMLHelper {
	public static String convertXMLtoHTML(String contentStringXML, String contentStringXSD, String contentStringXSLT) throws SAXException, ParserConfigurationException, IOException, TransformerException{
		InputSource xml = new InputSource(new StringReader(contentStringXML));

		StreamSource xsd = new StreamSource(new StringReader(contentStringXSD));

		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema;
		schema = schemaFactory.newSchema(xsd);
		// Construcción del parser del documento. Se establece el
		// esquema y se
		// activa la validación y comprobación de namespaces
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setNamespaceAware(true);
		factory.setSchema(schema);
		// Se añade el manejador de errores
		DocumentBuilder builder = factory.newDocumentBuilder();
		builder.setErrorHandler(new SimpleErrorHandler());

		Document xmlDocument = builder.parse(xml);


		TransformerFactory transformerfactory = TransformerFactory.newInstance();
		StreamSource xslt = new StreamSource(new StringReader(contentStringXSLT));
		Transformer transformer = transformerfactory.newTransformer(xslt);
		// El resultado se almacenará en una cadena de texto
		StringWriter result = new StringWriter();
		transformer.transform(new DOMSource(xmlDocument), new StreamResult(result));
		return result.toString();
	}
}
